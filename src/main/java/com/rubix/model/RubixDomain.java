package com.rubix.model;


import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class RubixDomain {

    private Date createdAt;
    private Date updatedAt;
    private String createdBy;
    private String updatedBy;
    private boolean archived = false;
}
