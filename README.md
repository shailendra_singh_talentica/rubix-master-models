# rubix-master-models


## How this works
- This repo is  public and owned by individual
- This repo has branches dedicated to each isloted group of models
- This repo uses [JitPack](https://jitpack.io/docs/) online build tool, which checkouts the branch and imports the jar.
- This can work with private repository as well, read [here](https://jitpack.io/private) more on JitPack

## How to import the models into dependent repositories

### Gradle

```build.gradle```

```
allprojects {
	repositories {
	    mavenCentral()
	    maven { url 'https://jitpack.io' }
        }
    }
```
```
dependencies {
   implementation 'org.bitbucket.username:repository:branch-SNAPSHOT'
}
```



## How to use the imported model as Mongo Document
### Example shows RiskFactor model to be used as Mongo Document

`RiskFactorDO.java`

```
@Document(collection = "riskfactor_model_poc")
@CompoundIndex(name = "riskfactor_index", def = "{'riskFactorName' : 1}", sparse = true, unique = true)
public class RiskFactorDO extends RiskFactor {


    @Override
    @Id
    public String getId() {
        return super.getId();
    }

}
```